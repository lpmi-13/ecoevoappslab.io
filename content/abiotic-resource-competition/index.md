---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Abiotic Resource Competition"
summary: "Tilman's resource competition model"
authors: ["Gaurav Kandlikar"]
tags: ["Species interactions"]
categories: []
date: 2020-07-19T22:15:26-07:00

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false
---

This app implements the abiotic resource competition model developed by Dave Tilman. Broadly, this model considers the dynamics between species that require the same set of abiotic resources (nutrients) for population growth. The consumers' population grows as they consume resources and convert them into population growth, and shrinks with some background mortality rates. The resource pools grow as resources enter the system from the outside, and shrinks as they are consumed by the consumer species. 

The model is described in more detail [here](https://cbs.umn.edu/sites/cbs.umn.edu/files/public/downloads/PopulusHelp_e.pdf) (see pg. 42 onwards). The app currently only implements **competition between two consumer species for two essential resources**. 

**Link to app**: https://ecoevoapps.shinyapps.io/abiotic_resource_competition/
