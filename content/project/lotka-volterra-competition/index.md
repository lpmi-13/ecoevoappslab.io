---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Lotka Volterra Competition"
summary: " "
authors: ["Kenji Hayashi"]
tags: ["Species interactions"]
categories: []
date: 2020-07-19T22:15:19-07:00

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

---

This app allows users to simulate the dynamics of the classic two-species [Lotka-Volterra competition](https://en.wikipedia.org/wiki/Competitive_Lotka%E2%80%93Volterra_equations) model. 

**Link to app**: https://ecoevoapps.shinyapps.io/lotka-volterra-competition/
