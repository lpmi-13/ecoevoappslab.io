---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Biotic Resource Competition"
summary: "Interactions between consumers (predators) that feed on the same biotic resource (prey)"
authors: ["Maddi Cowen", "Rosa McGuire"]
tags: ["Species interactions"]
categories: []
date: 2020-07-19T22:15:32-07:00


# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

---

This app simulates the dynamics between two consumers (e.g. predators or herbivores) that both consume the same biotic resource. The prey may be modeled as experiencing exponential or logistic growth, and the predators may be modeled as having a Type 1 or Type 2 functional response. 

**Link to app**: https://ecoevoapps.shinyapps.io/biotic-resource-competition/
