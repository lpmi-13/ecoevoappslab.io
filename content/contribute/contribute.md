+++
widget = "blank"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 15  # Order that this section will appear.

title = ""
subtitle = ""

[design]
  # Choose how many columns the section has. Valid values: 1 or 2.
  columns = "1"

[design.spacing]
  # Customize the section spacing. Order is top, right, bottom, left.
  padding = ["40px", "0", "20px", "0"]

+++

### Thanks for your interest in getting involved! We'd love to have your help.

There's many ways to get involved: 
  
1. [Join our mailing list](#join-our-mailing-list)
2. [Request a new app](#request-a-new-app)  
3. [Review existing apps](#review-existing-apps) 
4. [Contribute a new app](#contribute-a-new-app)

You can also email us at ecoevoapps@gmail.com with any questions.

All contributors are expected to follow the [Contributors Code of Conduct]({{< ref "/codeOfConduct.md" >}}). 

#### Join our mailing list
**[Join the EcoEvoApps mailing list]()** to receive updates on the project! We will send out periodic announcements when new apps become available or with other community news, but we promise we won't spam your inboxes :)

<iframe src="https://docs.google.com/forms/d/e/1FAIpQLSe2l99FTlXngfsaZFpxpnfxUSE6T-ehCsYfTmtz2YHQ7nV2Ww/viewform?embedded=true" width="1000" height="400" frameborder="0" marginheight="0" marginwidth="0">Loading…</iframe>

#### Request a new app

If you are a teacher or student and don't see an app that you'd like to explore, please let us know! You can request a new app on [this form](https://forms.gle/M483PXy1AVAYB9oq5), or get in touch with us via [email](mailto:ecoevoapps@gmail.com).

Please be as specific as possible in your request so we are sure the type of model you are requesting, and let us know if you have any R code that implements the model/analysis you are requesting (but it’s not required!).

#### Review existing apps

We are working to build a team of reviewers who can give feedback on existing apps (including checking for bugs, identifying potential enhancments, revising the model explanations, or reviewing the code). If you are interested in learing how to write Shiny apps, or if you're looking to learn a new model but don't want to write a whole app quite yet, this might be a great way to get involved. [Send us an email](mailto:ecoevoapps@gmail.com) if you'd like to help review some apps!

#### Contribute a new app

If you would like to contribute an app to EcoEvoApps, we'd love to hear from you! Our initial set of apps focus on fundamental theoretical models in population and community ecology, but we welcome any app that you think might be relavent to EEB students/researchers, including models that recreate data analyses from research papers. We are especially excited to grow our collection of apps that implement fundamental models from evolutionary biology. Please [email us](mailto:ecoevoapps@gmail.com) if you'd like to contribute an app to this project! 

*If you'd like to contribute an app but don't know where to start*, let us know and we will point you towards relavent resources!
