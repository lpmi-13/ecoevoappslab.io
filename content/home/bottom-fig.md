+++
widget = "blank"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 99  # Order that this section will appear.

title = ""
subtitle = ""

[design]
  # Choose how many columns the section has. Valid values: 1 or 2.
  columns = "1"

  # Text color (true=light or false=dark).
  text_color_light = false

[design.spacing]
  # Customize the section spacing. Order is top, right, bottom, left.
  padding = ["0px", "0", "0px", "0"]

+++
{{< figure library="true" src="mac-rosen.gif" title="Macarthur-Rosensweig model in the [consumer-resource dynamics app](project/predator-prey-dynamics)" >}} 
