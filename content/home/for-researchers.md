+++
widget = "blank"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 26  # Order that this section will appear.

title = ""
subtitle = ""

[design]
  # Choose how many columns the section has. Valid values: 1 or 2.
  columns = "1"

  # Text color (true=light or false=dark).
  text_color_light = false

[design.spacing]
  # Customize the section spacing. Order is top, right, bottom, left.
  padding = ["20px", "0", "20px", "0"]

+++

### For researchers

Interactive apps can be a great way to learn/review existing models, or to share your work with a wide audience of colleagues, students, and other members of the broader public. We invite you to [explore our existing models](#projects) and to [contribute new models]({{< ref "/contribute.md" >}}) to EcoEvoApps!

You can also **[join our mailing list](/contribute/#join-our-mailing-list)** to join the EcoEvoApps community and receive updates on the project.

